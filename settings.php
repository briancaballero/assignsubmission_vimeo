<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file defines the admin settings for this plugin
 *
 * @package   assignsubmission_vimeo
 * @copyright 2017 Kaplan AU
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$settings->add(new admin_setting_configtext('assignsubmission_vimeo/accesstoken',
                   new lang_string('accesstoken', 'assignsubmission_vimeo'),
                   new lang_string('accesstoken_help', 'assignsubmission_vimeo'), 'VIMEOACCESSTOKEN'));

$settings->add(new admin_setting_configcheckbox('assignsubmission_vimeo/default',
                   new lang_string('default', 'assignsubmission_vimeo'),
                   new lang_string('default_help', 'assignsubmission_vimeo'), 0));

$settings->add(new admin_setting_configtext('assignsubmission_vimeo/maxfiles',
                   new lang_string('maxfiles', 'assignsubmission_vimeo'),
                   new lang_string('maxfiles_help', 'assignsubmission_vimeo'), 5, PARAM_INT));



if (isset($CFG->maxbytes)) {

    $name = new lang_string('maximumsubmissionsize', 'assignsubmission_vimeo');
    $description = new lang_string('configmaxbytes', 'assignsubmission_vimeo');

    $maxbytes = get_config('assignsubmission_vimeo', 'maxbytes');
    $element = new admin_setting_configselect('assignsubmission_vimeo/maxbytes',
                                              $name,
                                              $description,
                                              262144000,
                                              array(
                                                  '104857600' => '100MB',
                                                  '262144000' => '250MB',
                                                  '524288000' => '500MB',
                                              ));
    $settings->add($element);
}