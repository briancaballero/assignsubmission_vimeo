// Standard license block omitted.
/*
 * @package    assignsubmission_vimeo
 * @copyright  2017 Kaplan AU
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
 
 /**
  * @module assignsubmission_vimeo/vimeo
  */

define(['jquery', 'jqueryui', 'assignsubmission_vimeo/vimeo-upload'], function($) {
    return {

        $fileList: null,
        $vimeoVideos: null,
        videoFiles: [],
        inProgress: false,
        uploadedVideos: [],
        deletedVideos: [],
        uploadedErrorVideos: [],
        $vimeoForm: null,
        limits: {},
        totalFileSize: 0,
        filesizes: {
            '104857600' : '100MB',
            '262144000' : '250MB',
            '524288000' : '500MB',
        },

        form: function(accessToken, albumId, courseShortname, assignId, assignName, accountName, fileCountLimit, fileSizeLimit) {
            var submitForm = this;

            // disable buttons to prevent processing while building UI for upload
            $('input[name="vimeo_ids"]')
                .parents('form.mform')
                .find('input[type="submit"]')
                .prop('disabled', true);

            submitForm.limits = {
                                    maxfiles: fileCountLimit,
                                    maxbytes: fileSizeLimit
                                };

            submitForm.totalFileSize = $('input[name="vimeo_ids"]').data('totalfilesize');
            
            // file input to mimic file open event
            var $mutipleFileInput = $('<input>')
                                        .attr('name', 'vimeo_videos[]')
                                        .attr('type', 'file')
                                        .attr('id', 'vimeo_videos_output')
                                        .attr('multiple', true)
                                        .attr('accept', '.mp4, .avi, .mov, .3gp, .mkv, .wmv, .flv, .mpg')
                                        .on('change', function(e){
                                            submitForm.listFiles(e.target.files);
                                        });

            // drag and drop file list
            submitForm.$fileList = $('<ul>')
                                    .attr('id', 'video_files')
                                    .click(function() {
                                        $('input[name="vimeo_videos[]"]').click();
                                    })
                                    .on('dragenter', function (e) 
                                    {
                                        e.stopPropagation();
                                        e.preventDefault();
                                    })
                                    .on('dragover', function (e) 
                                    {
                                         e.stopPropagation();
                                         e.preventDefault();
                                    })
                                    .on('drop', function(e) {
                                        e.preventDefault();
                                        submitForm.listFiles(e.originalEvent.dataTransfer.files);
                                    });
            submitForm.$fileList.append('<li class="placeholder">Choose files to upload or drag and drop them here.</li>');

            window.onbeforeunload = function () {
                if(submitForm.inProgress) {
                    return "You have attempted to leave this page while video(s) are still being processed. Are you sure?";
                }
            };

            // recently uploaded videos on edit mode/view (if assignment is not yet submitted)
            if ($('input[name="vimeo_ids"]').val().length > 0) {
                submitForm.$vimeoVideos = $('<ul>')
                                            .attr('id', 'vimeo_links');
                $('input[name="vimeo_ids"]').after(submitForm.$vimeoVideos);
                $('input[name="vimeo_ids"]').next().after(submitForm.$fileList);


                var vimeoIds = $('input[name="vimeo_ids"]').val().split('|');

                $.each(vimeoIds, function(index, vID) {
                    var $vimeoLinkRemove = $('<button class="remove-video">remove</button>')
                                            .click(function(e) {
                                                e.preventDefault();
                                                e.stopPropagation();

                                                if (submitForm.inProgress) {
                                                    return false;
                                                }

                                                $(this).parent().remove();
                                                submitForm.deletedVideos.push(vID);

                                                // deduct from total files uploaded
                                                var $vimeoData = $('input[name="vimeo_data"]');
                                                var videoData = $vimeoData.val().length > 0 ? JSON.parse($vimeoData.val()) : {};

                                                if (Object.keys(videoData).length) {
                                                    submitForm.totalFileSize -= parseInt(videoData[vID].size);
                                                }

                                                if ($('#vimeo_links li').length == 0) {
                                                    $('#vimeo_links').remove();
                                                }

                                            });

                    var $vimeoLinkRow = $('<li>')
                                    .attr('id', vID)
                                    .html('<a href="https://vimeo.com/'+vID+'" target="_blank">https://vimeo.com/'+vID+'</a>')
                                    .append($vimeoLinkRemove);

                    submitForm.$vimeoVideos.append($vimeoLinkRow);
                });

            } else {
                $('input[name="vimeo_ids"]').after(submitForm.$fileList);
            }

            submitForm.$fileList.after($mutipleFileInput);
            
            submitForm.$fileList.next().after('<br /><small class="vimeo-api-status"></small>');
            // var maxSize = submitForm.humanFileSize(submitForm.limits.maxbytes, true);
            var maxSize = submitForm.filesizes[submitForm.limits.maxbytes];
            submitForm.$fileList.next().after('<br /><small class="file-requirements">(Maximum size for new files: '+maxSize+', maximum attachments: '+submitForm.limits.maxfiles+')</small>');
            submitForm.$fileList.next().after('<small class="file-requirements">(*.mp4, *.avi, *.mov, *.3gp, *.mkv, *.flv, *.wmv)</small>');


            // form submission handler
        	$('input[name="vimeo_videos[]"]').parents('form.mform').on('submit', function(e) {
                if(submitForm.inProgress) {
                    return false; //do nothing since we are still processing videos
                }


                var action = $('input[type="submit"][clicked="true"]').val();

                submitForm.$vimeoForm = this;
                
                if(action !== 'Cancel') {
                    e.preventDefault();

                    if(submitForm.countVideoSubmissions() === 0) {
                        submitForm.$fileList.addClass('error');
                        submitForm.displayError('Video required.');
                        return;
                    }

                    // mark as in progress
                    submitForm.inProgress = true;

                    // disable buttons to prevent multiple process
                    $('input[name="vimeo_ids"]')
                        .parents('form.mform')
                        .find('input[type="submit"]')
                        .prop('disabled', true);

                    // delete videos
                    submitForm.delete(accessToken, function() {
                        for (var filehash in submitForm.videoFiles) {
                            var $fileProgressLabel = $('<div>Uploading...</div>')
                                                        .addClass('progress-label')
                                                        .css({
                                                            'position': 'absolute',
                                                            'left': '50%',
                                                        });

                            var $fileProgress = $('<div>')
                                                    .attr('id', 'file-progress_'+filehash)
                                                    .addClass('file-progress')
                                                    .append($fileProgressLabel);

                            $fileProgress.progressbar({
                                value: 0
                            });
                            
                            $('li#'+filehash).append($fileProgress);
                        }

                        submitForm.upload(accessToken, courseShortname, assignName, accountName, albumId, submitForm.$vimeoForm);
                    });
                }

            });

            // form button handler to determine if it's being submitted or cancelled
            $('input[name="vimeo_videos[]"]').parents('form.mform').find('input[type="submit"]').on('click', function() {
                $("input[type=submit]", $(this).parents("form")).removeAttr("clicked");
                $(this).attr("clicked", "true");
            });


            submitForm.createAlbum(accessToken, assignId, albumId, courseShortname, assignName, function() {
                $('input[name="vimeo_ids"]')
                    .parents('form.mform')
                    .find('input[type="submit"]')
                    .prop('disabled', false);
            });

        },

        listFiles: function(files) {
            var submitForm = this;

            submitForm.$fileList.removeClass('error');

            submitForm.$fileList.find('li.placeholder').remove();

            $.each(files, function(index, file) {
                if(submitForm.isFileQueued(file)) {
                    // submitForm.displayError(file.name + ' already added for queued.');
                    submitForm.displayError(M.util.get_string('alreadyqued', 'assignsubmission_vimeo', file.name));
                } else if(!submitForm.isVideo(file)) {
                    // submitForm.displayError(file.name + ' should be of video type.');
                    submitForm.displayError(M.util.get_string('shouldbevideo', 'assignsubmission_vimeo', file.name));
                } else if(!submitForm.canFit()) {
                    // submitForm.displayError('Max file of ' + submitForm.limits.maxfiles + ' already reached. Skipping ' + file.name + '.');
                    submitForm.displayError(window.M.util.get_string('numberoffilesexceed', 'assignsubmission_vimeo', {
                        numfiles: submitForm.limits.maxfiles,
                        filename: file.name
                    }));
                } else if(!submitForm.sizeFit(file.size)) {
                    // submitForm.displayError('Max upload limit reached. Skipping ' + file.name + '.');
                    submitForm.displayError(M.util.get_string('filesizeexceeded', 'assignsubmission_vimeo', submitForm.filesizes[submitForm.limits.maxbytes]));
                } else {
                    var fileHash = submitForm.createUniqueFileHash();

                    var $fileRemove = $('<button class="remove-video">x</button>')
                                        .click(function(e) {
                                            e.preventDefault();
                                            e.stopPropagation();

                                            if (submitForm.inProgress) {
                                                return false;
                                            }

                                            // remove filesize
                                            submitForm.totalFileSize -= submitForm.videoFiles[fileHash].size;

                                            delete submitForm.videoFiles[fileHash];
                                            
                                            if(Object.keys(submitForm.videoFiles).length === 0) {
                                                submitForm.$fileList.append('<li class="placeholder">Choose files to upload or drag and drop them here.</li>');
                                            }

                                            $(this).parent().remove();
                                        });

                    var $fileRow = $('<li>')
                                    .attr('id', fileHash)
                                    .data('name', file.name)
                                    .text(file.name)
                                    .append($fileRemove);

                    submitForm.$fileList.append($fileRow);
                    submitForm.videoFiles[fileHash] = file;
                    submitForm.totalFileSize += file.size;
                }
            });
        },

        isFileQueued: function(file) {
            var submitForm = this;

            return $.grep(submitForm.videoFiles, function(e) {
                return e.name == file.name;
            }).length > 0;
        },

        isVideo: function(file) {
            var submitForm = this;
            var ext = submitForm.getFileExtension(file.name);

            switch (ext.toLowerCase()) {
                case 'mov':
                case 'avi':
                case 'wmv':
                case 'flv':
                case 'mp4':
                case '3gp':
                case 'mkv':
                case 'mpg':
                    return true;
            }

            return false;
        },

        countVideoSubmissions: function() {
            var submitForm = this;
            var vimeoIds = $('input[name="vimeo_ids"]').val().length > 0 ? $('input[name="vimeo_ids"]').val().split('|') : [];
            return Object.keys(submitForm.videoFiles).length + vimeoIds.length - submitForm.deletedVideos.length;
        },

        canFit: function() {
            var submitForm = this;

            var vimeoIds = $('input[name="vimeo_ids"]').val().length > 0 ? $('input[name="vimeo_ids"]').val().split('|') : [];
            var totalFiles = Object.keys(submitForm.videoFiles).length + vimeoIds.length - submitForm.deletedVideos.length;

            return totalFiles < submitForm.limits.maxfiles;
        },

        sizeFit: function(filesize) {
            var submitForm = this;

            return submitForm.totalFileSize + filesize <= submitForm.limits.maxbytes;
        },

        getFileExtension: function(filename) {
            var parts = filename.split('.');
            return parts[parts.length - 1];
        },

        createUniqueFileHash: function() {
            return (new Date().getTime()).toString(36);
        },

        displayError: function(errMsg) {
            var $error = $('<div>')
                            .addClass('alert alert-danger alert-block fade in')
                            .attr('role', 'alert')
                            .text(errMsg)
                            .prepend('<button type="button" class="close" data-dismiss="alert">×</button>');
            $('#user-notifications').append($error);
        },

        displayNotice: function(msg) {
            var $error = $('<div>')
                            .addClass('alert alert-info alert-block fade in')
                            .attr('role', 'alert')
                            .text(msg)
                            .prepend('<button type="button" class="close" data-dismiss="alert">×</button>');
            $('#user-notifications').append($error);
        },

        addUploadError: function(errMsg) {
            var errors = $('input[name="upload_errors"]').val();

            if (errors.length) {
                errors += '|';
            }

            errors += errMsg;

            $('input[name="upload_errors"]').val(errors);
        },

        upload: function(accessToken, courseShortname, assignName, accountName, albumId, submissionForm) {
            var submitForm = this;

            $('small.vimeo-api-status').text('Uploading/Updating file(s)...');

            if(Object.keys(submitForm.videoFiles).length == submitForm.uploadedVideos.length + submitForm.uploadedErrorVideos.length) {
                submitForm.inProgress = false;
                $('small.vimeo-api-status').text('Process completed.');
                submissionForm.submit();
                return;
            }

            var currentFileHash = '';

            for (var filehash in submitForm.videoFiles) {
                if(currentFileHash.length === 0 && submitForm.uploadedVideos.indexOf(filehash) === -1) {
                    currentFileHash = filehash;
                }
            }

            var videoFile = submitForm.videoFiles[currentFileHash];
            var title = courseShortname + ' | ' + accountName + ' | ' + videoFile.name;
            var videoDescription = 'Course: ' + courseShortname + '\n';
                videoDescription += 'Assignment: ' + assignName + '\n';
                videoDescription += 'Student: ' + accountName + '\n';
                videoDescription += 'Video: ' + videoFile.name;
            var uploader = new VimeoUpload({
                name: title, 
                file: videoFile,
                token: accessToken,
                description: videoDescription,
                onComplete: function(data) {
                    if(typeof data === 'number') {
                        data = data.toString();
                    }

                    // check if error occured during upload of this video
                    if ($.inArray(currentFileHash, submitForm.uploadedErrorVideos) >= 0) {
                        $('#file-progress_'+currentFileHash).progressbar('value', 0);
                        $('#file-progress_'+currentFileHash).find('.progress-label').text('Upload Failed.');
                    } else {
                        var vimeoId = data;

                        $('#file-progress_'+currentFileHash).progressbar('value', 99.999999);
                    
                        // store video_ids
                        var videoIds = $('input[name="vimeo_ids"]').val();

                        if (videoIds.length) {
                            videoIds += '|' + vimeoId;
                        } else {
                            videoIds = vimeoId;
                        }

                        $('input[name="vimeo_ids"]').val(videoIds);

                        // store video_data
                        var videoData = $('input[name="vimeo_data"]').val().length > 0 ? JSON.parse($('input[name="vimeo_data"]').val()) : new Object();

                        if (typeof videoData[vimeoId] === 'undefined') {
                            videoData[vimeoId] = new Object();
                        }

                        videoData[vimeoId].size = videoFile.size;

                        $('input[name="vimeo_data"]').val(JSON.stringify(videoData));

                        submitForm.uploadedVideos.push(currentFileHash);

                        $('#file-progress_'+currentFileHash).find('.progress-label').text( "Setting Privacy..." );
                        submitForm.updateVideoPrivacy(accessToken, vimeoId, function() {
                            submitForm.setEmbedDomain(accessToken, vimeoId, function() {
                                $('#file-progress_'+currentFileHash).find('.progress-label').text( "Setting Album..." );
                                submitForm.addToAlbum(accessToken, albumId, vimeoId, function() {
                                    $('#file-progress_'+currentFileHash).progressbar('value', 100);
                                    $('#file-progress_'+currentFileHash).find('.progress-label').text( "100%" );
                                    submitForm.upload(accessToken, courseShortname, assignName, accountName, albumId, submissionForm);
                                });
                            });
                        });
                    }
                },
                onProgress: function(data) {
                    var uploadProgress = data.loaded / data.total;

                    if (isNaN(uploadProgress) || data.total == 0) {
                        $('#file-progress_'+currentFileHash).progressbar('value', 0);
                        $('#file-progress_'+currentFileHash).find('.progress-label').text('Upload Failed.');

                        // attempt to push error hoping to be handled onComplete
                        if ($.inArray(currentFileHash, submitForm.uploadedErrorVideos) < 0) {
                            submitForm.uploadedErrorVideos.push(currentFileHash);
                        }
                    } else {
                        $('#file-progress_'+currentFileHash).progressbar('value', uploadProgress * 100);
                        $('#file-progress_'+currentFileHash).find('.progress-label').text( Math.round(uploadProgress * 100) + "%" );
                    }
                },
                onError: function(data) {
                    var detailedError = '';

                    if (data.length == 0) {
                        detailedError = 'empty response';
                    } else {
                        var error = $.parseJSON(data);
                        detailedError = error.error;
                    }

                    $('#file-progress_'+currentFileHash).progressbar('value', 0);
                    $('#file-progress_'+currentFileHash).find('.progress-label').text('Upload Failed.');

                    if ($.inArray(currentFileHash, submitForm.uploadedErrorVideos) < 0) {
                        submitForm.uploadedErrorVideos.push(currentFileHash);
                    }

                    // if error occured while uploading display error message
                    submitForm.displayError('Error occured while uploading video ('+videoFile.name+'): ' + detailedError);
                    submitForm.addUploadError('Error occured while uploading video ('+videoFile.name+'): ' + detailedError);

                    submitForm.upload(accessToken, courseShortname, assignName, accountName, albumId, submissionForm);
                }
            });

            uploader.upload();
        },

        delete: function(accessToken, callback) {
            var submitForm = this;

            $('small.vimeo-api-status').text('Delete recent uploads...');

            if(submitForm.deletedVideos.length > 0) {
                var videoID = submitForm.deletedVideos[0];

                $.ajax({
                    beforeSend: function(request) {
                        request.setRequestHeader("Authorization", 'bearer ' + accessToken);
                    },
                    url: 'https://api.vimeo.com/videos/' + videoID,
                    method: 'DELETE',
                    success: function(data, status) {
                        submitForm.displayNotice('https://vimeo.com/'+videoID+' deleted.');
                    },
                    error: function(xhr, status, error) {
                        submitForm.displayError('Error occured while deleting video ('+videoID+'): ' + error);
                        submitForm.addUploadError('Error occured while deleting video ('+videoID+'): ' + error);
                    },
                    complete: function(xhr, status) {
                        if (submitForm.$vimeoVideos.children().length === 0) {
                            submitForm.$vimeoVideos.remove();
                        }

                        vimeoIds = $('input[name="vimeo_ids"]').val().split('|');
                        vimeoIds.splice(vimeoIds.indexOf(videoID), 1);

                        $('input[name="vimeo_ids"]').val(vimeoIds.join('|'));

                        submitForm.deletedVideos.splice(0, 1);

                        // remove from vimeo_data
                        var videoData = $('input[name="vimeo_data"]').val().length > 0 ? JSON.parse($('input[name="vimeo_data"]').val()) : {};

                        if (Object.keys(videoData).length) {
                            delete videoData[videoID];
                            $('input[name="vimeo_data"]').val(JSON.stringify(videoData));
                        }

                        submitForm.delete(accessToken, callback);
                    }
                });
            } else { // no more videos to delete
                if (typeof callback === 'function') {
                    callback();
                    return;
                }
            }

            return;
        },

        createAlbum: function(accessToken, assignId, albumId, courseShortname, assignName, callback) {
            var submitForm = this;

            if (albumId === 0) {
                albumId = parseInt($('input[name="vimeo_album_id"]').val());
            }

            if (albumId === 0) {

                var newAlbumId = 0;

                var albumDescription = 'Course: ' + courseShortname + '\n';
                    albumDescription += 'Assignment: ' + assignName;

                var albumPassword = submitForm.createUniqueFileHash();

                $.ajax({
                    beforeSend: function(request) {
                        request.setRequestHeader("Authorization", 'bearer ' + accessToken);
                    },
                    url: 'https://api.vimeo.com/me/albums',
                    method: 'POST',
                    data: {
                        name: courseShortname,
                        description: albumDescription,
                        privacy: 'password',
                        password: albumPassword
                    },
                    success: function(data, status) {
                        var uriChunks = data.uri.split('/');
                        newAlbumId = uriChunks[uriChunks.length - 1];
                        $('input[name="vimeo_album_id"]').val(newAlbumId);

                        // save new album
                        var pathChunks = location.pathname.split('/');
                        pathChunks.splice(pathChunks.length - 1, 1);
                        var endpoint = pathChunks.join('/') + '/submission/vimeo/';
                        $.ajax({
                            url: endpoint,
                            method: 'POST',
                            data: {
                                action: 'add_album',
                                tkn: accessToken,
                                assignid: assignId,
                                albumid: newAlbumId,
                                albumpassword: albumPassword
                            },
                            success: function(data, status) {
                                
                            },
                            error: function(xhr, status, error) {
                                
                            },
                            complete: function(xhr, status) {
                                if (typeof callback === 'function') {
                                    callback();
                                }
                            }
                        });
                    },
                    error: function(xhr, status, error) {
                        submitForm.displayError('Error occured while creating album: ' + error);
                    },
                    complete: function(xhr, status) {
                        
                    }
                });
            } else {
                if (typeof callback === 'function') {
                    callback();
                }
            }
        },

        addToAlbum: function(accessToken, albumId, vimeoId, callback) {
            var submitForm = this;

            if (albumId === 0) {
                albumId = $('input[name="vimeo_album_id"]').val();
            }

            $.ajax({
                beforeSend: function(request) {
                    request.setRequestHeader("Authorization", 'bearer ' + accessToken);
                },
                url: 'https://api.vimeo.com/me/albums/'+albumId+'/videos/'+vimeoId,
                method: 'PUT',
                success: function(data, status) {

                },
                error: function(xhr, status, error) {
                    submitForm.displayError('Error occured while adding video to album ('+albumId+'): ' + error);
                },
                complete: function(xhr, status) {
                    if (typeof callback === 'function') {
                        callback();
                    }
                }
            });
        },

        updateVideoPrivacy: function(accessToken, vimeoId, callback) {
            var submitForm = this;

            if (typeof vimeoId === 'number') {
                vimeoId = vimeoId.toString();
            }

            var videoPassword = submitForm.createUniqueFileHash();

            $.ajax({
                beforeSend: function(request) {
                    request.setRequestHeader("Authorization", 'bearer ' + accessToken);
                },
                url: 'https://api.vimeo.com/videos/'+vimeoId,
                method: 'PATCH',
                data: {
                    'privacy.add': false,
                    'privacy.comments': 'nobody',
                    'privacy.download': false,
                    'privacy.embed': 'whitelist',
                    'privacy.view': 'password',
                    'password': videoPassword
                },
                success: function(data, status) {
                    // store video password
                    var videoData = $('input[name="vimeo_data"]').val().length > 0 ? JSON.parse($('input[name="vimeo_data"]').val()) : {};

                    if (typeof videoData[vimeoId] === 'undefined') {
                        videoData[vimeoId] = {};
                    }

                    videoData[vimeoId].password = videoPassword;

                    $('input[name="vimeo_data"]').val(JSON.stringify(videoData));
                },
                error: function(xhr, status, error) {
                    submitForm.displayError('Error occured while updating video privacy: ' + error);
                },
                complete: function(xhr, status) {
                    if (typeof callback === 'function') {
                        callback();
                    }
                }
            });
        },

        setEmbedDomain: function(accessToken, vimeoId, callback) {
            var submitForm = this;

            $.ajax({
                beforeSend: function(request) {
                    request.setRequestHeader("Authorization", 'bearer ' + accessToken);
                },
                url: 'https://api.vimeo.com/videos/'+vimeoId+'/privacy/domains/'+window.location.host,
                method: 'PUT',
                success: function(data, status) {

                },
                error: function(xhr, status, error) {
                    submitForm.displayError('Error occured while updating video privacy domain: ' + error);
                },
                complete: function(xhr, status) {
                    if (typeof callback === 'function') {
                        callback();
                    }
                }
            });
        },

        humanFileSize: function(bytes, si) {
            var thresh = si ? 1000 : 1024;
            if(Math.abs(bytes) < thresh) {
                return bytes + ' B';
            }
            var units = si
                ? ['kB','MB','GB','TB','PB','EB','ZB','YB']
                : ['KiB','MiB','GiB','TiB','PiB','EiB','ZiB','YiB'];
            var u = -1;
            do {
                bytes /= thresh;
                ++u;
            } while(Math.abs(bytes) >= thresh && u < units.length - 1);
            return bytes.toFixed(1)+' '+units[u];
        }
    };
});
