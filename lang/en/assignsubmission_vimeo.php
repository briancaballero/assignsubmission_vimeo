<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'assignsubmission_vimeo', language 'en'
 *
 * @package   assignsubmission_vimeo
 * @copyright 2017 Kaplan AU
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

$string['allowonlinetextsubmissions'] = 'Enabled';
$string['default'] = 'Enabled by default';
$string['default_help'] = 'If set, this submission method will be enabled by default for all new assignments.';
$string['enabled'] = 'Vimeo';
$string['enabled_help'] = 'If enabled, students are able to attach videos to be uploaded on Vimeo Account.';
$string['vimeosubmission'] = 'Allow vimeo submission';
$string['pluginname'] = 'Vimeo submissions';
$string['vimeo'] = 'Vimeo';
$string['video'] = 'Video';
$string['video_file'] = 'Video File(s)';
$string['maxfiles'] = 'Maximum videos per submission';
$string['maxfiles_help'] = 'If vimeo submissions are enabled, each assignment can be set to accept up to this number of videos for their submission.';
$string['maxfilessubmission'] = 'Maximum number of uploaded videos';
$string['maxfilessubmission_help'] = 'If vimeo submissions are enabled, each student will be able to upload up to this number of video files for their submission.';
$string['maximumsubmissionsize'] = 'Maximum video submission size';
$string['maximumsubmissionsize_help'] = 'Videos uploaded by students may be up to this size.';
$string['configmaxbytes'] = 'Maximum file size';
$string['clientid'] = 'Vimeo App Client Identifier';
$string['clientid_help'] = 'Client Identifier for API request authentication';
$string['clientsecret'] = 'Vimeo App Client Secret';
$string['clientsecret_help'] = 'Client Secret for API request authentication';
$string['accesstoken'] = 'Vimeo App Access Token';
$string['accesstoken_help'] = 'Access token for API requests.  Should have "upload edit" scope.';
$string['novideouploaded'] = 'No video uploaded.';
$string['albumpassword'] = 'Album Password';
$string['albumlink'] = 'Vimeo Album Link';

// client strings
$string['alreadyqued'] = '{$a} already added for queued.';
$string['shouldbevideo'] = '{$a} should be of video type.';
$string['numberoffilesexceed'] = 'Max file of {$a->numfiles} already reached. Skipping {$a->filename}.';
$string['filesizeexceeded'] = 'Upload exceeds maximum supported file size of {$a}. Please lower the resolution of your video or contact your lecturer for advice.';
