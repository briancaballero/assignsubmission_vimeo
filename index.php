<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * AJAX cater
 * responds to POST request
 * requires AccessToken from plugin config
 *
 * @package    assignsubmission_vimeo
 * @copyright 2017 Kaplan AU
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

// load moodle
require_once('../../../../config.php');
require_once($CFG->dirroot . '/mod/assign/locallib.php');

if ($_SERVER['REQUEST_METHOD'] !== 'POST') exit();

if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
	// find the access token

	if (!isset($_POST['tkn']) || empty($_POST['tkn'])) {
		exit();
	}

	// check if accesstoken is valid
	$accesstoken = get_config('assignsubmission_vimeo', 'accesstoken');

	if ($accesstoken !== $_POST['tkn']) {
		exit();
	}

	if (!isset($_POST['action']) || empty($_POST['action'])) {
		exit();
	}

	$action = trim($_POST['action']);

	switch ($action) {
		case 'add_album':
			if (!isset($_POST['albumid']) || empty($_POST['albumid'])) {
				echo json_encode(array(
						'status' => 'error',
						'message' => 'Album ID required'
					));
				break;
			}

			if (!isset($_POST['assignid']) || empty($_POST['assignid'])) {
				echo json_encode(array(
						'status' => 'error',
						'message' => 'Assign ID required'
					));
				break;
			}

			if (!isset($_POST['albumpassword']) || empty($_POST['albumpassword'])) {
				echo json_encode(array(
						'status' => 'error',
						'message' => 'Album password required'
					));
				break;
			}

			add_album($_POST['albumid'], $_POST['assignid'], $_POST['albumpassword']);
			break;
		
		default:
			exit();
			break;
	}
}

function add_album($albumid, $assignid, $password) {
	global $DB;

	if ($vimeoalbum = $DB->get_record('assignsubmission_vimeo_album', array('assignid' => $assignid))) {
		if ($vimeoalbum->albumid != $albumid) {
			echo json_encode(array(
					'status' => 'error',
					'message' => 'Album already exists for assignment.'
				));
		} else {
			echo json_encode(array(
					'status' => 'error',
					'message' => 'Album already created for assignment.'
				));
		}
		
		return;
	}

	$vimeoalbum = new stdClass();
	$vimeoalbum->assignid = $assignid;
	$vimeoalbum->albumid = $albumid;
	$vimeoalbum->password = $password;
	$vimeoalbum->timecreated = time();
    $vimeoalbum->id = $DB->insert_record('assignsubmission_vimeo_album', $vimeoalbum);

    echo json_encode(array(
					'status' => 'success',
					'message' => 'Album recorded for assignment.'
				));

    return;
}