<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Upgrade code for install
 *
 * @package   assignsubmission_vimeo
 * @copyright 2017 Kaplan AU
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();

/**
 * Stub for upgrade code
 * @param int $oldversion
 * @return bool
 */
function xmldb_assignsubmission_vimeo_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    if ($oldversion < 2017072400) {

        // Define table assignsubmission_vimeo_album to be created.
        $table = new xmldb_table('assignsubmission_vimeo_album');

        // Adding fields to table assignsubmission_vimeo_album.
        $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
        $table->add_field('assignid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('albumid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');
        $table->add_field('timecreated', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, null, '0');

        // Adding keys to table assignsubmission_vimeo_album.
        $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
        $table->add_key('assignment', XMLDB_KEY_FOREIGN, array('assignid'), 'assign', array('id'));

        // Conditionally launch create table for assignsubmission_vimeo_album.
        if (!$dbman->table_exists($table)) {
            $dbman->create_table($table);
        }

        // Vimeo savepoint reached.
        upgrade_plugin_savepoint(true, 2017072400, 'assignsubmission', 'vimeo');
    }

    if ($oldversion < 2017073100) {

        // Define field password to be added to assignsubmission_vimeo.
        $table = new xmldb_table('assignsubmission_vimeo');
        $field = new xmldb_field('password', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null, 'filesize');

        // Conditionally launch add field password.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Vimeo savepoint reached.
        upgrade_plugin_savepoint(true, 2017073100, 'assignsubmission', 'vimeo');
    }

    if ($oldversion < 2017110600) {
        
        // Define field password to be added to assignsubmission_vimeo_album.
        $table = new xmldb_table('assignsubmission_vimeo_album');
        $field = new xmldb_field('password', XMLDB_TYPE_CHAR, '20', null, XMLDB_NOTNULL, null, null, 'albumid');

        // Conditionally launch add field password.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Vimeo savepoint reached.
        upgrade_plugin_savepoint(true, 2017110600, 'assignsubmission', 'vimeo');
    }


    return true;
}
