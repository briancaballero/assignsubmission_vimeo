"use strict";
 
module.exports = function (grunt) {
    require("grunt-load-gruntfile")(grunt);
    grunt.loadGruntfile("../../../../Gruntfile.js");

    grunt.initConfig({
        watch: {
            css: {
                files: 'amd/src/sass/*.scss',
                tasks: ['sass']
            },
        },

        sass: {
            dist: {
                option: {
                    style: 'compressed',
                },
                files: {
                    'styles.css' : 'amd/src/sass/vimeo.scss'
                }
            },
        },

        copy: {
            main: {
                files: [
                    {
                        expand: true,
                        src: 'vimeo-upload.js',
                        cwd: 'node_modules/vimeo-upload',
                        dest: 'amd/src/'
                    }
                ]
            }
        }
    });

    grunt.loadNpmTasks("grunt-contrib-sass");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks("grunt-contrib-clean");
    grunt.loadNpmTasks("grunt-contrib-copy");

    grunt.registerTask('default', ['sass', 'copy']);
};