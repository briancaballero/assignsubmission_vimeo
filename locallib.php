<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file contains the definition for the library class for onlinetext submission plugin
 *
 * This class provides all the functionality for the new assign module.
 *
 * @package assignsubmission_vimeo
 * @copyright 2017 Kaplan AU
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

define('ASSIGNSUBMISSION_VIMEO_FILEAREA', 'submission_vimeos');

class assign_submission_vimeo extends assign_submission_plugin {

    /**
     * Get the name of the online text submission plugin
     * @return string
     */
    public function get_name() {
        return get_string('vimeo', 'assignsubmission_vimeo');
    }

    /**
     * Get vimeo submission information from the database
     *
     * @param  int $submissionid
     * @param  int $vimeoid
     * @return mixed
     */
    private function get_vimeo_submission($submissionid, $vimeoid) {
        global $DB;

        return $DB->get_record('assignsubmission_vimeo', array('submissionid'=>$submissionid, 'vimeoid' => $vimeoid));
    }

    /**
     * Get vimeo submission list from the database
     *
     * @param  int $submissionid
     * @return mixed
     */
    private function get_vimeo_submissions($submissionid) {
        global $DB;

        return $DB->get_records('assignsubmission_vimeo', array('submissionid'=>$submissionid));
    }

    private function get_vimeo_submission_ids($submissionid) {
        global $DB;

        return $DB->get_fieldset_sql('SELECT vimeoid FROM {assignsubmission_vimeo} WHERE submissionid = ?', array($submissionid));
    }

    private function count_vimeo_submissions($submissionid) {
        global $DB;

        return $DB->count_records('assignsubmission_vimeo', array('submissionid'=>$submissionid));
    }


    /**
     * Get the default setting for file submission plugin
     *
     * @param MoodleQuickForm $mform The form to add elements to
     * @return void
     */
    public function get_settings(MoodleQuickForm $mform) {
        global $CFG, $COURSE;

        $defaultmaxfilesubmissions = 5;
        $defaultmaxsubmissionsizebytes = '262144000';

        $settings = array();
        $options = array();
        for ($i = 1; $i <= get_config('assignsubmission_vimeo', 'maxfiles'); $i++) {
            $options[$i] = $i;
        }

        $name = get_string('maxfilessubmission', 'assignsubmission_vimeo');
        $mform->addElement('select', 'assignsubmission_vimeo_maxfiles', $name, $options);
        $mform->addHelpButton('assignsubmission_vimeo_maxfiles',
                              'maxfilessubmission',
                              'assignsubmission_vimeo');
        if(get_config('assignsubmission_vimeo', 'maxfiles')) {
            $defaultmaxfilesubmissions = get_config('assignsubmission_vimeo', 'maxfiles');
        }
        $mform->setDefault('assignsubmission_vimeo_maxfiles', $defaultmaxfilesubmissions);
        $mform->disabledIf('assignsubmission_vimeo_maxfiles', 'assignsubmission_vimeo_enabled', 'notchecked');

        $choices = array(
                        '104857600' => '100MB',
                        '262144000' => '250MB',
                        '524288000' => '500MB',
                    );

        $name = get_string('maximumsubmissionsize', 'assignsubmission_vimeo');
        $mform->addElement('select', 'assignsubmission_vimeo_maxsizebytes', $name, $choices);
        $mform->addHelpButton('assignsubmission_vimeo_maxsizebytes',
                              'maximumsubmissionsize',
                              'assignsubmission_vimeo');
        if(get_config('assignsubmission_vimeo', 'maxbytes')) {
            $defaultmaxsubmissionsizebytes = get_config('assignsubmission_vimeo', 'maxbytes');
        }
        $mform->setDefault('assignsubmission_vimeo_maxsizebytes', $defaultmaxsubmissionsizebytes);
        $mform->disabledIf('assignsubmission_vimeo_maxsizebytes',
                           'assignsubmission_vimeo_enabled',
                           'notchecked');
    }

    /**
     * Save the settings for file submission plugin
     *
     * @param stdClass $data
     * @return bool
     */
    public function save_settings(stdClass $data) {
        $this->set_config('maxfilesubmissions', $data->assignsubmission_vimeo_maxfiles);
        $this->set_config('maxsubmissionsizebytes', $data->assignsubmission_vimeo_maxsizebytes);
        return true;
    }

    /**
     * Add form elements for settings
     *
     * @param mixed $submission can be null
     * @param MoodleQuickForm $mform
     * @param stdClass $data
     * @return true if elements were added to the form
     */
    public function get_form_elements($submission, MoodleQuickForm $mform, stdClass $data) {
        global $PAGE, $USER, $DB, $COURSE;

        $accesstoken = get_config('assignsubmission_vimeo', 'accesstoken');
        $maxfiles = $this->get_config('maxfilesubmissions');
        $maxbytes = $this->get_config('maxsubmissionsizebytes');

        if($maxbytes==0) {
            $upload_max_filesize = ini_get('upload_max_filesize');
            $unit = preg_replace('/[^bkmgtpezy]/i', '', $upload_max_filesize);
            $size = preg_replace('/[^0-9\.]/', '', $upload_max_filesize);
            if($unit) {
                $max_filesize = round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
            } else {
                $max_filesize = round($size);
            }

            $maxbytes = $max_filesize;
        }

        $accountname = fullname($USER, true);
        $assignname = $this->assignment->get_instance()->name;
        $assignid = $this->assignment->get_instance()->id;
        $courseshortname = $COURSE->shortname;

        if($album = $DB->get_record('assignsubmission_vimeo_album', array('assignid'=>$assignid))) {
            $albumid = $album->albumid;
        } else {
            $albumid = 0;
        }

        $submissionid = $submission ? $submission->id : 0;
        $vimeo_ids_value = '';
        $totalfilesize = 0;
        $vimeo_data = array();
        if ($submissionid > 0) {
            $vimeosubmissions = $this->get_vimeo_submissions($submissionid);

            foreach ($vimeosubmissions as $submission) {
                if (!empty($vimeo_ids_value)) {
                    $vimeo_ids_value .= '|';
                }

                $vimeo_ids_value .= $submission->vimeoid;
                $totalfilesize += $submission->filesize;

                $vimeo_data[$submission->vimeoid] = array('size'=>$submission->filesize);
            }
        }

        $PAGE->requires->js_call_amd('assignsubmission_vimeo/submitform', 'form', array($accesstoken, $albumid, $courseshortname, $assignid, $assignname, $accountname, $maxfiles, $maxbytes));

        $mform->addElement('text', 'vimeo_ids', get_string('video', 'assignsubmission_vimeo'), array('data-totalfilesize' => $totalfilesize));
        $mform->setType('vimeo_ids', PARAM_RAW);
        $mform->addElement('text', 'vimeo_data', $this->get_name() . ' Data');
        $mform->setType('vimeo_data', PARAM_RAW);
        if(!empty($vimeo_data)) {
            $mform->setDefault('vimeo_data', json_encode($vimeo_data));
        }
        $mform->addElement('hidden', 'vimeo_album_id', $this->get_name() . ' Album');
        $mform->setType('vimeo_album_id', PARAM_RAW);
        $mform->setDefault('vimeo_album_id', $albumid);

        $mform->addElement('hidden', 'upload_errors', $this->get_name() . ' Upload Errors');
        $mform->setType('upload_errors', PARAM_RAW);
        $mform->setDefault('upload_errors', '');

        if (!empty($vimeo_ids_value)) {
            $mform->setDefault('vimeo_ids', $vimeo_ids_value);
        }

        // load lang files
        $stringman = get_string_manager();
        $strings = $stringman->load_component_strings('assignsubmission_vimeo', 'en');
        $PAGE->requires->strings_for_js(array_keys($strings), 'assignsubmission_vimeo');

        return true;
    }

    /**
     * Save data to the database
     *
     * @param stdClass $submission
     * @param stdClass $data
     * @return bool
     */
    public function save(stdClass $submission, stdClass $data) {
        global $DB, $OUTPUT;

        $uploaded_videos = $data->vimeo_ids;
        $uploaded_videos_data = $data->vimeo_data;
        $vimeo_album_id = $data->vimeo_album_id;
        $upload_errors = $data->upload_errors;

        if (!empty($uploaded_videos_data)) {
            $vimeo_data = json_decode($uploaded_videos_data, true);
        } else {
            $vimeo_data = [];
        }

        if (!empty($upload_errors)) {
            $upload_errors = explode('|', $upload_errors);

            foreach ($upload_errors as $error_msg) {
                $this->set_error($OUTPUT->error_text($error_msg));
            }
        }

        if($album = $DB->get_record('assignsubmission_vimeo_album', array('assignid' => $this->assignment->get_instance()->id))) {
            $albumid = $album->albumid;

            if($albumid != $vimeo_album_id) { // Oops! somehow the album was changed, need to use what is on data
                $vimeo_album_id = $albumid;
            }
        } else {
            // keep track of the album id for future uploads
            $vimeoalbum = new stdClass();
            $vimeoalbum->assignid = $this->assignment->get_instance()->id;
            $vimeoalbum->albumid = $vimeo_album_id;
            $vimeoalbum->timecreated = time();
            $vimeoalbum->id = $DB->insert_record('assignsubmission_vimeo_album', $vimeoalbum);
        }

        // remove vimeo records that was removed by student during edit
        if (!empty($uploaded_videos)) {
            $DB->execute('DELETE FROM {assignsubmission_vimeo} WHERE submissionid = ' . $submission->id . ' AND vimeoid NOT IN (' . str_replace('|', ',', $uploaded_videos) . ')');
        } else {
            $DB->execute('DELETE FROM {assignsubmission_vimeo} WHERE submissionid = ' . $submission->id);
        }

        if (empty($uploaded_videos) && empty($vimeo_data)) {
            $this->set_error($OUTPUT->error_text(get_string('novideouploaded', 'assignsubmission_vimeo')));
            return false;
        }

        $videoids = explode('|', $uploaded_videos);

        foreach ($videoids as $videoid) {
            $vimeosubmission = $this->get_vimeo_submission($submission->id, $videoid);

            if($vimeosubmission) { // automatically update knowing that the student edited the draft submission
                $vimeosubmission->timemodified = time();
                $updatestatus = $DB->update_record('assignsubmission_vimeo', $vimeosubmission);
            } else {
                $vimeosubmission = new stdClass();
                $vimeosubmission->submissionid = $submission->id;
                $vimeosubmission->assignid = $this->assignment->get_instance()->id;
                $vimeosubmission->vimeoid = $videoid;

                if(isset($vimeo_data[$videoid]) && isset($vimeo_data[$videoid]['size'])) {
                    $vimeosubmission->filesize = $vimeo_data[$videoid]['size'];
                } else {
                    $vimeosubmission->filesize = 0;
                }

                if(isset($vimeo_data[$videoid]) && isset($vimeo_data[$videoid]['password'])) {
                    $vimeosubmission->password = $vimeo_data[$videoid]['password'];
                } else {
                    $vimeosubmission->password = '';
                }

                $vimeosubmission->timecreated = time();
                $vimeosubmission->timemodified = time();
                $vimeosubmission->id = $DB->insert_record('assignsubmission_vimeo', $vimeosubmission);
            }
        }

        return true;
    }

    /**
      * Display Vimeo links of uploaded videos
      *
      * @param stdClass $submission
      * @param bool $showviewlink - If the summary has been truncated set this to true
      * @return string
      */
    public function view_summary(stdClass $submission, & $showviewlink) {
        $vimeosubmissions = $this->get_vimeo_submissions($submission->id);

        if (!empty($vimeosubmissions)) {
            $showviewlink = true;
            
            $vimeo_links = '<ul>';

            foreach ($vimeosubmissions as $submission) {
                $vimeo_links .= '<li>';
                $vimeo_links .= '<a href="https://vimeo.com/'.$submission->vimeoid.'" target="_blank">https://vimeo.com/'.$submission->vimeoid.'</a>';


                if (!empty($submission->password)) {
                    $vimeo_links .= ' [password: ' .$submission->password. ']';
                }

                $vimeo_links .= '</li>';
            }

            $vimeo_links .= '</ul>';

            return $vimeo_links;
        }

        return '';
    }

    /**
     * Display the saved text content from the editor in the view table
     *
     * @param stdClass $submission
     * @return string
     */
    public function view(stdClass $submission) {
        $oembed_endpoint = 'http://vimeo.com/api/oembed';

        $vimeosubmissions = $this->get_vimeo_submissions($submission->id);

        if (!empty($vimeosubmissions)) {
            $vimeo_links = '<ul class="vimeo-list-preview">';

            foreach ($vimeosubmissions as $submission) {
                $vimeo_links .= '<li>';

                $video_url = 'http://vimeo.com/' . $submission->vimeoid;
                $json_url = $oembed_endpoint . '.json?url=' . rawurlencode($video_url).'&width=320';
                $response = $this->curl_get($json_url);
                $oembed_data = json_decode($response);

                if (!is_object($oembed_data)) {
                    $vimeo_links .= '<br />Video processing on <a href="http://vimeo.com">vimeo.com</a>';
                } else {
                    $vimeo_links .= html_entity_decode($oembed_data->html);

                    if (!empty($submission->password)) {
                        $vimeo_links .= '<br /><small>Video password: ' .$submission->password.'</small>';
                    }
                }

                $vimeo_links .= '</li>';
            }

            $vimeo_links .= '</ul>';

            return $vimeo_links;
        }

        return '';
    }

    /**
     * Display the album details for vimeo submissions
     *
     * @param stdClass $submission
     * @return string
     */

    public function view_header() {
        global $DB;
        $id = required_param('id', PARAM_INT);
        list ($course, $cm) = get_course_and_cm_from_cmid($id, 'assign');
        $context = context_module::instance($cm->id);
        $albumdetails = '';

        if(has_capability('mod/assign:grade', $context)) {
            if ($album = $DB->get_record('assignsubmission_vimeo_album', array('assignid' => $cm->instance))) {
                $albumdetails .= '<div class="box generalbox boxaligncenter"> '.get_string('albumlink', 'assignsubmission_vimeo');
                $albumdetails .= ': <a href="http://vimeo.com/album/' . $album->albumid . '" target="_blank">http://vimeo.com/album/' . $album->albumid . '</a>';
                $albumdetails .= '</div>';
                $albumdetails .= '<div class="box generalbox boxaligncenter"> '.get_string('albumpassword', 'assignsubmission_vimeo');
                $albumdetails .= ': ' . $album->password;
                $albumdetails .= '</div>';
            }
        }

        return $albumdetails;
    }

    private function curl_get($url) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        $return = curl_exec($curl);
        curl_close($curl);
        return $return;
    }

    /**
     * Formatting for log info
     *
     * @param stdClass $submission The new submission
     * @return string
     */
    public function format_for_log(stdClass $submission) {
        
        $vimeosubmissions = $this->get_vimeo_submissions($submission->id);

        $vimeo_log = '';

        if (!empty($vimeosubmissions)) {
            foreach ($vimeosubmissions as $submission) {
                $vimeo_log .= $submission->vimeoid . '|';
            }
        }


        return $vimeo_log;
    }

    /**
     * The assignment has been deleted - cleanup
     *
     * @return bool
     */
    public function delete_instance() {
        global $DB;
        $DB->delete_records('assignsubmission_vimeo',
                            array('assignid'=>$this->assignment->get_instance()->id));

        return true;
    }

    /**
     * No video is posted for this plugin
     *
     * @param stdClass $submission
     * @return bool
     */
    public function is_empty(stdClass $submission) {
        return $this->count_vimeo_submissions($submission->id) == 0;
    }

    /**
     * Get file areas returns a list of areas this plugin stores files
     * @return array - An array of fileareas (keys) and descriptions (values)
     */
    public function get_file_areas() {
        return array(ASSIGNSUBMISSION_VIMEO_FILEAREA=>$this->get_name());
    }


    /**
     * Copy the student's submission from a previous submission. Used when a student opts to base their resubmission
     * on the last submission.
     * @param stdClass $sourcesubmission
     * @param stdClass $destsubmission
     */
    public function copy_submission(stdClass $sourcesubmission, stdClass $destsubmission) {
        global $DB;

        $vimeosubmissions = $this->get_vimeo_submissions($sourcesubmission->id);

        if (!empty($vimeosubmissions)) {
            foreach ($vimeosubmissions as $vimeosubmission) {
                unset($vimeosubmission->id);
                $vimeosubmission->submissionid = $destsubmission->id;
                $vimeosubmission->timecreated = time();
                $vimeosubmission->timemodified = time();
                $DB->insert_record('assignsubmission_vimeo', $vimeosubmission);
            }
        }

        return true;
    }

    /**
     * Return a description of external params suitable for posting vimeo from a webservice.
     *
     * @return external_description|null
     */
    public function get_external_parameters() {
        return array(
            'vimeo_ids' => new external_value(
                PARAM_INT,
                'Vimeo ids (pipe delimited)',
                VALUE_OPTIONAL
            )
        );
    }
}